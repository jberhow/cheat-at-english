import java.io.*;
import java.util.*;
import java.util.regex.*;

public class Main {
	
	public static Set<String> dict;
		
	public static void main(String[] args) throws FileNotFoundException {

		dict = buildDict();
		int i = 0;

		File file = new File("GenKill.txt");
		Map<String, Integer> words = filter(parse(file, 6), 4);
		
		System.out.println("Generation Kill:\n");

		for (String str : words.keySet()) {
			if (i < 4) {
				System.out.print(str + ": " + words.get(str) + "\t");
				i++;
			}
			else {
				i = 0;
				System.out.println();
			}
		}

		System.out.println();
		System.out.println("\nThe Things they Carried:\n");

		file = new File("TheThings.txt");
		words = filter(parse(file, 6), 4);

		for (String str : words.keySet()) {
			if (i < 4) {
				System.out.print(str + ": " + words.get(str) + "\t");
				i++;
			}
			else {
				i = 0;
				System.out.println();
			}
		}

		System.out.println();
		System.out.println("\nWhere Men Win Glory:\n");

		file = new File("WhereMen.txt");
		words = filter(parse(file, 6), 4);

		for (String str : words.keySet()) {
			if (i < 4) {
				System.out.print(str + ": " + words.get(str) + "\t");
				i++;
			}
			else {
				i = 0;
				System.out.println();
			}
		}
		
		System.out.println();

	}

	private static Set<String> buildDict() throws FileNotFoundException {
		
		Set<String> dict = new HashSet<>();
		File file = new File("dictionary.txt");
		Scanner scanner = new Scanner(file);
	
		while (scanner.hasNext()) {
			dict.add(scanner.next());
		}
		return dict;
	
	}

	public static Map<String, Integer> parse(File file, int wordLen) throws FileNotFoundException {
	
		Map<String, Integer> words = new HashMap<>();
		Scanner scanner = new Scanner(file);

		while (scanner.hasNext()) {
			String temp = scanner.next();
			if (dict.contains(temp) && temp.length() > wordLen) {
				if (words.containsKey(temp)) {
					words.put(temp, words.get(temp) + 1);
				}
				else {
					words.put(temp, 1);
				}
			}
			
		}
		return words;
	}

	public static Map<String, Integer> filter(Map<String, Integer> words, int wordFreq) {
	
		Map<String, Integer> temp = new HashMap<String, Integer>();
		int i = 0;

		for (String word : words.keySet()) {
			if (words.get(word) < wordFreq) {
				temp.put(word, words.get(word));
				if (i > 3) {
					i = 0;
				}
				i++;
			}
		}
		return temp;

	}

	public static ArrayList<String> sentenize(File file, Map<String, Integer> words) throws FileNotFoundException {
		
		ArrayList<String> output = new ArrayList<>();
		Pattern re;
		Matcher reMatcher;
		String bookHolder = "";
		Scanner scanner = new Scanner(file);

		while (scanner.hasNext()) {
			bookHolder += scanner.next();
		}

		for (String word : words.keySet()) {
			re = Pattern.compile("([^\\.!?]*\\b" + word + "\\b[^\\.!?]*)[\\.!?]");
			reMatcher = re.matcher(bookHolder);
			if (reMatcher.find()) {
				output.add(reMatcher.group());
			}
		}
		return output;

	}
}
